#!/bin/bash

DEFAULT_KAFKA="192.168.0.100"
ARG1=${1:-KAFKA_BROKER=$DEFAULT_KAFKA}

sudo make clean && sudo capstan rmi rule-engine && sudo capstan run -e '--env='$ARG1' /python.so /rule-engine/rule_engine.py' -n bridge
