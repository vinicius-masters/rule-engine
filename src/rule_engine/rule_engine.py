import operator
import json

from time import time
from os import environ
from collections import defaultdict
from pyjung import JungRegistry, JungTasker


class RuleEngine(JungRegistry):
    """ Manage simple IFTTT rules for single devices.

    An instance of RuleEngine should monitor devices readings and trigger a command
    when suitable, besides processing its own new tasks. In order to so, it overrides
    the JungRegistry method process_tasks, which is originally made to only run anything
    when new tasks arrive.
    """

    def __init__(self, kafka_broker, in_topic, out_topic, group, port='9092'):
        super().__init__(kafka_broker, in_topic, out_topic, port, group)
        self.reading_tasker = JungTasker(in_topic="reading_results",
                                         out_topic="reading_tasks",
                                         ip=kafka_broker, port=port, group=group)
        self.command_tasker = JungTasker(in_topic="command_results",
                                         out_topic="command_tasks",
                                         ip=kafka_broker, port=port, group=group)
        self.operators = {
            "<": operator.le,
            ">": operator.gt,
            "==": operator.eq,
            "!=": operator.ne
        }

    def evaluate_rule(self, rule, reading):
        condition = rule["condition"]
        target = rule["target"]

        return self.operators[condition](reading["value"], target)

    def process_message(self, message):
        task_id = message["id"]
        task = message["task"]
        device_id = message["content"]["device_id"]

        response = {"task_id": task_id, "task": task}
        result = None

        if task == "CREATE":
            self.storage[device_id] = message["content"]
            result = {"created": self.storage[device_id]}
        elif task == "GET":
            if device_id in self.storage:
                result = self.storage[device_id]
            else:
                result = {"error": "this device has no rules"}
        response["result"] = result
        return response

    def process_tasks(self):
        last_check = 0
        while True:
            #check readings every second
            if time() - last_check >= 1:
                for device_id, rule in self.storage.items():
                    reading_req = {"device_id": device_id, "page_limit":1}
                    reading_req = self.tasker.create_task("READ", reading_req)
                    
                    self.reading_tasker.publish(reading_req)
                    
                    reading = None
                    while reading is None:
                        reading = self.reading_tasker.get_task_result(reading_req["id"])

                    #check if there is a reading and evaluate it
                    if len(reading) > 0 and not self.evaluate_rule(rule, reading[0]):
                        command = {"device_id":device_id,"command":rule["command"]}
                        cmd_task = self.tasker.create_task("COMMAND", command)
                        self.command_tasker.publish(cmd_task)
                        print("triggered:", rule)
                        print("command:", cmd_task)
                    
                    last_check = time()
            
            for offset_message in self.tasker.consumer.get_messages(count=1,
                                                                    block=True,
                                                                    timeout=0.03):
                message = json.loads(offset_message.message.value)
                try:
                    response = self.process_message(message)
                except Exception:
                    response = {"error": "malformed message"}
                print("response:\n {}".format(response))
                self.tasker.publish(response)


def main():
    kafka_broker = environ["KAFKA_BROKER"]
    rule_engine = RuleEngine(kafka_broker=kafka_broker,
                             in_topic="rule_tasks",
                             out_topic="rule_results",
                             port="9092",
                             group="rule-engine")
    rule_engine.process_tasks()


if __name__ == "__main__":
    main()
